package com.apiTest.bayarPbb.utilities;

import org.apache.commons.lang3.RandomStringUtils;

public class RestUtils {
	public static String nameGenerator() {
		String capital = RandomStringUtils.randomAlphabetic(1).toUpperCase();
		String generateString = RandomStringUtils.randomAlphabetic(4);
		generateString = "Gilbert "+ generateString;
		return generateString+capital;
	}
	public static String gmailGenerator() {
		String generateString1 = RandomStringUtils.randomAlphabetic(4);
		String generateString2 = RandomStringUtils.randomAlphabetic(4);
		String gmail = generateString1 + "@"+ generateString2+".com";
		return gmail;
	}
	public static String passwordGenerator() {
		String capital = RandomStringUtils.randomAlphabetic(1).toUpperCase();
		String lowercase=RandomStringUtils.randomAlphabetic(6).toLowerCase();
		int number = Integer.parseInt(RandomStringUtils.randomNumeric(1));
		return capital+lowercase+"@"+number;
	}
	public static String telephoneNumber(String telephone) {
		if(telephone.equals("+6281354882755"))
		{
			return "+6281341360209";
		}
		return "+6281354882755";
	}

	public static String taxNumberGenerator() {
		String taxNumber = RandomStringUtils.randomNumeric(18);
		return taxNumber;
	}
	public static String amountGenerator() {
		int number = Integer.parseInt(RandomStringUtils.randomNumeric(5));
		number = number%50000;
		number = number + 200000;
		return (number+".0");
	}
	public static int yearGenerator() {
		int number = Integer.parseInt(RandomStringUtils.randomNumeric(1));
		number = number%3;
		number = number + 2018;
		return number;
	}
	public static int bankAccountGenerator() {
		int number = Integer.parseInt(RandomStringUtils.randomNumeric(5));
		return number;
	}
	public static String bankUsernameGenerator() {
		String bankUsername = RandomStringUtils.randomAlphabetic(6);
		return bankUsername;
	}
	public static int idCityGenerator() {
		return 3;
	}
}
