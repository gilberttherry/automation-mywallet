package com.apiTest.bayarPbb.login.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Valid_And_Registered_Data extends bayarPbb{
	
	@BeforeClass
	private void PostLoginData() throws InterruptedException{
		logger.info("******** Started Login TC_001_Post_With_Valid_And_Registered_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		requestParams.put("email", akun.getProperty("email"));
		requestParams.put("password", akun.getProperty("password"));
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/login");

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		setToken();
		setIdUser();
		setBalance();
		akun.setProperty("idUser",Integer.toString(getIdUser()));
		akun.setProperty("token",getToken());
		akun.setProperty("balance", Double.toString(getBalance()));
		
		setProperties(akun, "Akun.properties");
		
		String responseBody =response.getBody().asString();
		Assert.assertTrue(responseBody.contains(akun.getProperty("name")));
		Assert.assertTrue(responseBody.contains(akun.getProperty("telephone")));
		Assert.assertTrue(responseBody.contains(akun.getProperty("email")));
		checkResponseBody("You are logged-in.");
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCodeParam) {
		checkStatusCode(successStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
//	
//	@Test
//	private void checkServerTypePost() {
//		checkServerType("null");
//	}
//	
//	@Test
//	private void checkContentEncodingPost() {
//		checkContentEncoding("null");
//	}
//	
	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished Login TC_001_Post_With_Valid_And_Registered_Data *********");
		
	}
}
