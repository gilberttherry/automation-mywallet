package com.apiTest.bayarPbb.login.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_002_Post_With_UnValidated_User extends bayarPbb{
	
	@BeforeClass
	private void PostLoginData() throws InterruptedException{
		logger.info("******** Started Login TC_003_Post_With_UnValidated_User *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		requestParams.put("email", akun.getProperty("email"));
		requestParams.put("password", akun.getProperty("password"));
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/login");

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
//		Properties akun = new Properties();
//		akun = getProperties("Akun.properties");
//		
//		setToken();
//		setIdUser();
//		setBalance();
//		akun.setProperty("idUser",Integer.toString(getIdUser()));
//		akun.setProperty("token",getToken());
//		akun.setProperty("balance", Double.toString(getBalance()));
//		
//		setProperties(akun, "Akun.properties");
//		
//		String responseBody =response.getBody().asString();
//		Assert.assertTrue(responseBody.contains(akun.getProperty("name")));
//		Assert.assertTrue(responseBody.contains(akun.getProperty("telephone")));
//		Assert.assertTrue(responseBody.contains(akun.getProperty("email")));
		checkResponseBody("This account is not verified yet.");
	}
	
	
	@Test
	private void checkStatusCodePost() {
		checkStatusCode("400");
	}
	
	@Test
	private void checkResponseTimePost() {
		checkResponseTime("5000");
		
	}

	@Test
	private void checkStatusLinePost() {
		checkStatusLine("HTTP/1.1 200 ");
	}
	
	@Test
	private void checkContentTypePost() {
		checkContentType("application/json");
	}
//	
//	@Test
//	private void checkServerTypePost() {
//		checkServerType("null");
//	}
//	
//	@Test
//	private void checkContentEncodingPost() {
//		checkContentEncoding("null");
//	}
//	
	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished Login TC_003_Post_With_UnValidated_User *********");
		
	}
}
