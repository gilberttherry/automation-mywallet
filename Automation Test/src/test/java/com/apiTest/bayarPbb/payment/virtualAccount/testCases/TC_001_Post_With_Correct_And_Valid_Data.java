package com.apiTest.bayarPbb.payment.virtualAccount.testCases;

import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Correct_And_Valid_Data extends bayarPbb{
	private int idBank = (Integer.parseInt(RandomStringUtils.randomNumeric(1))%3+1);
	@BeforeClass
	private void postPaymentVirtualAccount() throws InterruptedException{
		logger.info("******** Payment with Virtual Account Started TC_001_Post_With_Correct_And_Valid_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();

		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		
		requestParams.put("amount", Double.parseDouble(transaction.getProperty("payAmount")));
		
		Properties tax = new Properties();
		tax = getProperties("Tax.properties");
		
		requestParams.put("service", tax.getProperty("service"));
		requestParams.put("taxNumber", tax.getProperty("taxNumber"));
		requestParams.put("year", Long.parseLong(tax.getProperty("year")));
		requestParams.put("idBank", idBank);
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		response = httpRequest.request(Method.POST, "/user/"+akun.getProperty("idUser")+"/transaction-by-virtual-account?token="+akun.getProperty("token"));
		//System.out.println(requestParams.toString());
		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
		setVirtualNumber();
		setIdTransaction();
		setPayAmount();;
		
		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		transaction.setProperty("amount", Double.toString(Math.floor(getPayAmount())));
		transaction.setProperty("idBank", Integer.toString(idBank));
		transaction.setProperty("virtualNumber", getVirtualNumber());
		transaction.setProperty("idTransaction", Integer.toString(getIdTransaction()));
		transaction.setProperty("rest", Double.toString(getAmount()%1000));
		
		setProperties(transaction, "Transaction.properties");
		
		
		checkResponseBody("Create Transaction Success");
	}
	
	
	@Test
	@Parameters("postStatusCode")
	private void checkStatusCodePost(String postStatusCodeParam) {
		checkStatusCode(postStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Payment With Virtual Account Finished TC_001_Post_With_Correct_And_Valid_Data *********");
		
	}
}
