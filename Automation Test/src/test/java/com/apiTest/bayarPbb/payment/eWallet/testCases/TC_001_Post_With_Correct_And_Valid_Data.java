package com.apiTest.bayarPbb.payment.eWallet.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Correct_And_Valid_Data extends bayarPbb{
	
	@BeforeClass
	private void PaymentWithEwallet() throws InterruptedException{
		logger.info("******** Payment with E-Wallet Started TC_001_Post_With_Correct_And_Valid_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();

		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		
		requestParams.put("amount", Double.parseDouble(transaction.getProperty("payAmount")));
		

		Properties tax = new Properties();
		tax = getProperties("Tax.properties");
		requestParams.put("service", tax.getProperty("service"));
		requestParams.put("taxNumber", tax.getProperty("taxNumber"));
		requestParams.put("year", Long.parseLong(tax.getProperty("year")));
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		response = httpRequest.request(Method.POST, "/user/"+akun.getProperty("idUser")+"/transaction-by-ewallet?token="+akun.getProperty("token"));
		System.out.println(requestParams.toString());
		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		checkResponseBody("Create Transaction success");
	}
	
	
	@Test
	@Parameters("postStatusCode")
	private void checkStatusCodePost(String postStatusCodeParam) {
		checkStatusCode(postStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Payment With E-Wallet Finished TC_001_Post_With_Correct_And_Valid_Data *********");
		
	}
}
