package com.apiTest.bayarPbb.bank.virtualPayment.testCases;

import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;
import com.apiTest.bayarPbb.utilities.RestUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Valid_And_Correct_Data extends bayarPbb{
	private int idTransaction = Integer.parseInt(RandomStringUtils.randomNumeric(6));
	@BeforeClass
	private void postPaymentBank() throws InterruptedException{
		logger.info("******** Payment to bank Started TC_001_Post_With_Valid_And_Correct_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();


		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		requestParams.put("bankIdTransaction", idTransaction);
		requestParams.put("virtualNumber", transaction.getProperty("virtualNumber"));
		requestParams.put("bankAccount", Integer.toString(RestUtils.bankAccountGenerator()));
		requestParams.put("bankUsername", RestUtils.bankUsernameGenerator());
		requestParams.put("payAmount", Double.parseDouble(transaction.getProperty("amount")));
		

		Properties bankTransaction = new Properties();
		bankTransaction = getProperties("BankTransaction.properties");
		requestParams.put("idBank", Integer.parseInt(bankTransaction.getProperty("idBank")));
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());

		Properties bankToken = new Properties();
		bankToken = getProperties("BankToken.properties");
		if(Integer.parseInt(bankTransaction.getProperty("idBank"))==1)
		{
			response = httpRequest.request(Method.POST, "/pay-with-virtual-payment?token="+bankToken.getProperty("token1"));
		}
		else if(Integer.parseInt(bankTransaction.getProperty("idBank"))==2)
		{
			response = httpRequest.request(Method.POST, "/pay-with-virtual-payment?token="+bankToken.getProperty("token2"));
		}
		else if(Integer.parseInt(bankTransaction.getProperty("idBank"))==3)
		{
			response = httpRequest.request(Method.POST, "/pay-with-virtual-payment?token="+bankToken.getProperty("token3"));
		}
		System.out.println(requestParams.toString());
		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		
		setVirtualNumber();
		setIdTransaction();
		transaction.setProperty("virtualNumber", getVirtualNumber());
		transaction.setProperty("idTransaction", Integer.toString(getIdTransaction()));
		setProperties(transaction, "Transaction.properties");
		
		checkResponseBody("Transaction successfull");
	}
	
	
	@Test
	@Parameters("postStatusCode")
	private void checkStatusCodePost(String postStatusCodeParam) {
		checkStatusCode(postStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Payment With Virtual Account Finished TC_001_Post_With_Correct_And_Valid_Data *********");
		
	}
}
