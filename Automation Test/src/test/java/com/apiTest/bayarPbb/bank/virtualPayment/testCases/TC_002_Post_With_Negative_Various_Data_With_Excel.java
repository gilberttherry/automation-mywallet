package com.apiTest.bayarPbb.bank.virtualPayment.testCases;

import java.io.IOException;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;
import com.apiTest.bayarPbb.utilities.Utility;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_002_Post_With_Negative_Various_Data_With_Excel extends bayarPbb{
	
	@BeforeClass
	private void postLoginData() throws InterruptedException{
		logger.info("******** Payment to bank Started TC_002_Post_With_Negative_Various_Data_With_Excel *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();

		Thread.sleep(3);
	}
	
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataemployes", priority = 0)
	private void dataDrivenPost(String bankIdTransaction, String virtualNumber, String payAmount, String idBank, String bankAccount, String bankUsername) throws InterruptedException {
		httpRequest = RestAssured.given();

		JSONObject requestParams = new JSONObject();

		Properties bankToken = new Properties();
		bankToken = getProperties("BankToken.properties");

		requestParams.put("bankIdTransaction", bankIdTransaction);
		requestParams.put("virtualNumber", virtualNumber);
		requestParams.put("payAmount", Double.parseDouble(payAmount));
		requestParams.put("idBank", Integer.parseInt(idBank));
		requestParams.put("bankAccount", bankAccount);
		requestParams.put("bankUsername", bankUsername);
		
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(requestParams.toJSONString());
		
		
		if(Integer.parseInt(idBank)==1)
		{
			response = httpRequest.request(Method.POST, "/pay-with-virtual-payment?token="+bankToken.getProperty("token1"));
		}
		else if(Integer.parseInt(idBank)==2)
		{
			response = httpRequest.request(Method.POST, "/pay-with-virtual-payment?token="+bankToken.getProperty("token2"));
		}
		else if(Integer.parseInt(idBank)==3)
		{
			response = httpRequest.request(Method.POST, "/pay-with-virtual-payment?token="+bankToken.getProperty("token3"));
		}
		
		Thread.sleep(5);
	}
	
	@DataProvider(name="dataemployes")
	private String[][] getDataEmpty() throws IOException {
		String path = "/Users/therryg/eclipse-workspace/FinalProject/src/test/java/com/apiTest/bayarPbb/bank/virtualPayment/testCases/Negative_Data.xlsx";
		int rownum =Utility.getRowCount(path, "Sheet1");
		int colcount = Utility.getCellCount(path, "Sheet1",1);
		String dataEmployes [][] = new String[rownum][colcount];
		for(int i=1;i<=rownum;i++) {
			for(int j=0;j<colcount;j++) {
				dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
			}
		}
		return(dataEmployes);
	}

	@Test(priority = 1)
	private void checkResponseBodyPost() {
		checkResponseBodyDynamicPostLogin("");
	}
	
	
	@Test(priority = 1)
	private void checkStatusCodePost() {
		checkStatusCode("500");
	}
	
	@Test(priority = 1)
	private void checkResponseTimePost() {
		checkResponseTime("5000");
		
	}

	@Test(priority = 1)
	private void checkStatusLinePost() {
		checkStatusLine("HTTP/1.1 200 ");
	}
	
	@Test(priority = 1)
	private void checkContentTypePost() {
		checkContentType("application/json");
	}
	
//	@Test(priority = 1)
//	private void checkServerTypePost() {
//		checkServerType("\0");
//	}
	
//	@Test(priority = 1)
//	private void checkContentEncodingPost() {
//		checkContentEncoding("\0");
//	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Finished Payment to bank TC_002_Post_With_Negative_Various_Data_With_Excel *********");
		
	}
}
