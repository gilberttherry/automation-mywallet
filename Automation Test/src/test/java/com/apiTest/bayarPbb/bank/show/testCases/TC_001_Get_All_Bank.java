package com.apiTest.bayarPbb.bank.show.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_All_Bank extends bayarPbb{
	@BeforeClass
	private void PostRegistrationData() throws InterruptedException{
		logger.info("******** Show Bank Data Started TC_001_Get_All_Bank *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/bank");

		Thread.sleep(5);
	}
	@Test
	private void checkResponseBodyPost() {
		checkResponseBody("Bank BNI");
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successstatuscodeparam) {
		checkStatusCode(successstatuscodeparam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String timeParam) {
		checkResponseTime(timeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("100");
	}
	
//	@Test
//	private void checkJsonValidator() {
//		jsonValidator("/com/apiTest/bayarPbb/bank/show/testCases/Bank.json");
//	}
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Show Bill Data Finished TC_001_Get_All_Bank *********");
		
	}
}
