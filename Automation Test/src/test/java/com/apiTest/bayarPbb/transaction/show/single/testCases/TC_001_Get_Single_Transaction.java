package com.apiTest.bayarPbb.transaction.show.single.testCases;

import java.util.Properties;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_Single_Transaction extends bayarPbb{
	@BeforeClass
	private void getSingleTransaction() throws InterruptedException{
		logger.info("******** Show Single Transaction Data Started TC_001_Get_Single_Tranaction *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();

		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		response = httpRequest.request(Method.GET, "/user/"+akun.getProperty("idUser")+"/transaction/"+transaction.getProperty("idTransaction")+"?token="+akun.getProperty("token"));

		Thread.sleep(5);
	}
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
		checkResponseBody("Transaction founded");
	}
	

	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCode) {
		checkStatusCode(successStatusCode);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentTypeParam")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("250");
	}
	
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Show Single Transaction Data Finished TC_001_Get_Single_Tranaction *********");
		
	}
}
