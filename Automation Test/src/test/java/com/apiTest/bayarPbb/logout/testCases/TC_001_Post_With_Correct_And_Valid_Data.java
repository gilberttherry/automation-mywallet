package com.apiTest.bayarPbb.logout.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Correct_And_Valid_Data extends bayarPbb{

	@BeforeClass
	private void PostLoginData() throws InterruptedException{
		logger.info("******** Started Logout TC_001_Post_With_Correct_And_Valid_Data *********");
		RestAssured.baseURI = baseUrlString;
		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/user/"+akun.getProperty("idUser")+"/logout?token="+akun.getProperty("token"));

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		checkResponseBody("You are logged-out");
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCodeParam) {
		checkStatusCode(successStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine("HTTP/1.1 200 ");
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
//	
//	@Test
//	private void checkServerTypePost() {
//		checkServerType("null");
//	}
//	
//	@Test
//	private void checkContentEncodingPost() {
//		checkContentEncoding("null");
//	}
//	
	@Test
	private void checkContentLength() {
		checkContentLength("30");
	}
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished Logout TC_001_Post_With_Correct_And_Valid_Data *********");
		
	}
}
