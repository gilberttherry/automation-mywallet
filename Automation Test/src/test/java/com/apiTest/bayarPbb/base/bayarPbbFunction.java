package com.apiTest.bayarPbb.base;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.Assert;

import io.restassured.path.json.JsonPath;

public class bayarPbbFunction extends bayarPbb {
	public void nameValid() {
		JsonPath jsonPath = response.jsonPath();
		String name = jsonPath.get("name");
		logger.info(name);
		
		String regex = "/^[A-Za-z]+/";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(name);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
}
