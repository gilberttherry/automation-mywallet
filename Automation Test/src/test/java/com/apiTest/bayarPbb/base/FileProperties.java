package com.apiTest.bayarPbb.base;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class FileProperties {

	public FileProperties(Properties p,String nameFile){
		try {
			OutputStream output = new FileOutputStream(nameFile);
			p.store(output,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
