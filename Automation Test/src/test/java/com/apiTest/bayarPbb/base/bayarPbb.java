package com.apiTest.bayarPbb.base;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.PropertyConfigurator;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class bayarPbb {
	protected static RequestSpecification httpRequest;
	protected static Response response;
	public String baseUrlString = "http://localhost:9709";
	private int idUser=18;
	private String name="User Testing";
	private String email="email.dummy@gmail.com";
	private String password="P@ssw0rd";
	private String token="2bee8655-9d2c-448f-9008-9deb783fdfe3";
	private int idCity=6;
	private double amount;
	private int idProvince=6;
	private String provinceName;
	private String cityName;
	private double balance;
	private String pathProperties="/Users/therryg/eclipse-workspace/FinalProject/src/main/java/FinalProject/FinalProject/properties/";
	private int idTransaction=33;
	private int year=2017;
	private int pin =999999;
	private String noTelepon="+6281123456789";
	private String taxNumber="968574389786758437";
	private String bankIdTransaction = "23443542";
	private String virtualNumber;
	private double payAmount = 200000;
	private int idBank = 1;
	private String bankAccount="24353456456";
	private String bankUsername= "Gilbert Lijaya Therry";
	private String service="pbb";
	private Properties prop= new Properties();
	public double getBalance() {
		return balance;
	}
	public void setBalance() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		double balance = jsonObject.getDouble("balance");
		logger.info("user balance ===>"+balance);
		this.balance = balance;
	}

	@BeforeClass
	//@Parameters("baseURI")
	public void setup() {
		//baseUrlString = baseUriParam;
		logger = Logger.getLogger("E-Wallet");
		PropertyConfigurator.configure("Log4j.properties");
	}
	public void setProperties(Properties p, String nameFile) {
		try {
			OutputStream output = new FileOutputStream(pathProperties+nameFile);
			p.store(output,null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public Properties getProperties(String nameFile) {
		try {
			FileReader reader = new FileReader(pathProperties+nameFile);
			this.prop.load(reader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return this.prop;
	}
	public int getIdTransaction() {
		return idTransaction;
	}
	public void setIdTransaction() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		int idTransaction = jsonObject.getInt("idTransaction");
		logger.info("Transaction Id ===>"+idTransaction);
		this.idTransaction = idTransaction;
	}

	public void setAmount() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		double amount = jsonObject.getInt("amount");
		logger.info("amount  ===>"+amount);
		this.amount = amount;
	}
	public double getAmount() {
		return this.amount;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getBankIdTransaction() {
		return bankIdTransaction;
	}
	public void setBankIdTransaction(String bankIdTransaction) {
		this.bankIdTransaction = bankIdTransaction;
	}
	public String getVirtualNumber() {
		return virtualNumber;
	}
	public void setVirtualNumber() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		String virtualNumber = jsonObject.getString("virtualNumber");
		logger.info("Virtual Number ===>"+virtualNumber);
		this.virtualNumber = virtualNumber;
	}
	public double getPayAmount() {
		return payAmount;
	}
	public void setPayAmount() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		double payAmount = jsonObject.getDouble("payAmount");
		logger.info("Pay Amount ===>"+payAmount);
		this.payAmount = payAmount;
	}
	public int getIdBank() {
		return idBank;
	}
	public void setIdBank(int idBank) {
		this.idBank = idBank;
	}
	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}
	public String getBankUsername() {
		return bankUsername;
	}
	public void setBankUsername(String bankUsername) {
		this.bankUsername = bankUsername;
	}
	public String getName() {
		return name;
	}
	public int getPin() {
		return pin;
	}
	public void setPin() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		int pin1 = jsonObject.getInt("pin");
		logger.info("user pin ===>"+pin1);
		this.pin = pin1;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getIdUser() {
		return idUser;
	}
	public String getTaxNumber() {
		return taxNumber;
	}
	public void setTaxNumber(String taxNumber) {
		this.taxNumber = taxNumber;
	}
	public int getIdCity() {
		return idCity;
	}
	public void setIdCity(int idCity) {
		this.idCity = idCity;
	}
	public int getIdProvince() {
		return idProvince;
	}
	public void setIdProvince(int idProvince) {
		this.idProvince = idProvince;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void setIdUser() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		JSONObject jsonObject = new JSONObject(data);
		int idUser = jsonObject.getInt("idUser");
		logger.info("idUser  ===>"+idUser);
		this.idUser = idUser;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail() {
		JsonPath jsonPath = response.jsonPath();
		String email = jsonPath.get("email");

		logger.info("user email ===>"+email);
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getToken() {
		return token;
	}
	public void setToken() {
		JsonPath jsonPath = response.jsonPath();
		String data = jsonPath.get("data").toString();
		data=data.replace("/PhotoProfile/", "");
		System.out.println(data);
		JSONObject jsonObject = new JSONObject(data);
		String token = jsonObject.getString("token");
		logger.info("user token ===>"+token);
		this.token=token;
		
	}
	public String getNoTelepon() {
		return noTelepon;
	}
	public void setNoTelepon(String noTelepon) {
		this.noTelepon = noTelepon;
	}
	protected Logger logger;
	
	public void emailValid() {
		JsonPath jsonPath = response.jsonPath();
		String email = jsonPath.get("email");
		logger.info(email);
		
		String regex = "^(.+)@(.+)$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}

	public void checkEmptyBody() {
		logger.info("******** Checking is Response Body Empty *********");

		String responseBody = response.getBody().asString();
		logger.info("response Body ===> "+responseBody);
		Assert.assertTrue(responseBody != null);
	}
	
	public void checkStatusCode(String statusCodeParam) {
		logger.info("******** Checking Status Code *********");

		JsonPath jsonPath = response.jsonPath();
		String status = jsonPath.get("status").toString();
		status = status.replaceAll("[^0-9]", "");
		logger.info("Status code ===> "+ status);
		Assert.assertEquals(Integer.parseInt(status), Integer.parseInt(statusCodeParam));
//		int statusCode = response.getStatusCode();
//		logger.info("Status Code ===> "+ statusCode);
//		Assert.assertEquals(statusCode, Integer.parseInt(statusCodeParam));
	}
	public void checkBalance() {
		this.balance = 0.0;
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		double balance = Double.parseDouble(akun.getProperty("balance"));
		JsonPath jsonPath = response.jsonPath();
		HashMap<String, String> data = jsonPath.get("data");
		//System.out.println(data.toString());
		String transactions = data.toString().replace(":", "");
		JSONObject object = new JSONObject(transactions);
		System.out.println(object.get("transactions"));
		JSONArray array = new JSONArray(object.get("transactions").toString());
		//System.out.println(array);
		JSONObject jsonObject = new JSONObject(array);
		
		for (int i = 0; i < array.length(); i++) {
			jsonObject = array.getJSONObject(i);
			System.out.println(array.length()+jsonObject.toString());
			if((jsonObject.getInt("isFinished")==1)&&(jsonObject.getString("service").equals("TOPUP")))
			{
				this.balance = this.balance + jsonObject.getDouble("amount");
				logger.info("Balance after finished transaction with id "+ jsonObject.getInt("idTransaction")+" with service "+jsonObject.getString("service")+" ==>"+this.balance);
			}
			else if ((jsonObject.getInt("isFinished")==1)&&(jsonObject.getInt("is_use_wallet")==1))
			{
				this.balance = this.balance - jsonObject.getDouble("amount");
				logger.info("Balance after finished transaction with id "+ jsonObject.getInt("idTransaction")+" with service "+jsonObject.getString("service")+" ==>"+this.balance);
			}
		}
		Assert.assertEquals(this.balance, balance);
		logger.info("Final Balance ===> "+this.balance);
	}
	public void checkStatusCodeNormal(String statusCodeParam)
	{

		int statusCode = response.getStatusCode();
		logger.info("Status Code ===> "+ statusCode);
		Assert.assertEquals(statusCode, Integer.parseInt(statusCodeParam));
	}
	public void checkResponseBody(String response) {
		logger.info("******** Checking Response Body *********");
		
		String responseBody = this.response.getBody().asString();
		logger.info("Response Body ===> "+ responseBody);
		Assert.assertEquals(responseBody.contains(response), true);
	}
	public void getAttribute() {
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		String email=akun.getProperty("email");
		JsonPath jsonPath = response.jsonPath();
		HashMap<String, String> data = jsonPath.get("data");
		//System.out.println(data.toString());
		String users = data.toString();
		JSONObject object = new JSONObject(users);
		//System.out.println(object.get("users"));
		JSONArray array = new JSONArray(object.get("users").toString());
		//System.out.println(array);
		JSONObject jsonObject = new JSONObject(array);

		String responseBody = response.getBody().asString();
		
		for (int i = 0; i < array.length(); i++) {
			jsonObject = array.getJSONObject(i);
			//System.out.println(array.length()+jsonObject.toString());
			if(jsonObject.toString().contains(email))
			{
				//if(!jsonObject.get("otp").equals(null))
				//{
					akun.setProperty("otp", Integer.toString(jsonObject.getInt(("otp"))));
					logger.info("user otp ===>"+jsonObject.getInt("otp"));
				//}
				akun.setProperty("tokenEmail", jsonObject.getString("tokenEmail"));
				logger.info("user token email ===>"+jsonObject.getString("tokenEmail"));
				if(jsonObject.get("photoProfile")!=null)
				{
					akun.setProperty("photoProfile", jsonObject.get("photoProfile").toString());

					logger.info("user photo profile ===>"+jsonObject.get("photoProfile").toString());
				}
				akun.setProperty("balance", Integer.toString(jsonObject.getInt(("balance"))));
				logger.info("user photo profile ===>"+jsonObject.getInt("balance"));

				setProperties(akun, "Akun.properties");
				//System.out.println(jsonObject.toString());
			}
		}
		setProperties(akun, "Akun.properties");
	}
	
	public void checkResponseBodyDynamicPostLogin(String body) {
		logger.info("******** Checking Response Body Dynamic Post Login *********");
		
		String responseBody = response.getBody().asString();
		Assert.assertEquals(responseBody.contains(body), true);
	}
	public void checkResponseTime(String responseTimeParam) {
		logger.info("******** Checking Response Time *********");
		
		long responseTime = response.getTime();
		logger.info("Response time ===> "+ responseTime);
		Assert.assertTrue(responseTime < Integer.parseInt(responseTimeParam));
	}
	
	public void checkStatusLine(String statusLineParam) {
		logger.info("******** Checking Status Line *********");
		
		String statusLine = response.getStatusLine();
		logger.info("Status line ===> " + statusLine);
		Assert.assertEquals(statusLine, statusLineParam);
	}
	public void checkContentType(String contentTypeParam) {
		logger.info("******** Checking Content Type *********");
		
		String contentType = response.header("Content-Type");
		logger.info("Content-Type ===>" + contentType);
		Assert.assertEquals(contentType, contentTypeParam);
	}
	public void checkServerType(String serverTypeParam) {
		logger.info("******** Checking Server Type *********");
		
		String serverType = response.header("Server");
		logger.info("Server Type ===>" + serverType);
		Assert.assertEquals(serverType, serverTypeParam);
	}
	public void checkContentEncoding(String contentEncodingParam) {
		logger.info("******** Checking Content Encoding *********");
		
		String contentEncoding = response.header("Content-Encoding");
		logger.info("Content Encoding ===> "+ contentEncoding);
		Assert.assertEquals(contentEncoding, contentEncodingParam);
	}
	public void checkContentLength(String contentLengthParam) {
		logger.info("******** Checking Content Length *********");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content-Length ===> " + contentLength);
		if(Integer.parseInt(contentLength) < Integer.parseInt(contentLengthParam))
			logger.warning("Content Length is less than 50");
		Assert.assertTrue(Integer.parseInt(contentLength) >= Integer.parseInt(contentLengthParam));
	}
	public void checkContentLengthAll(String contentLengthParam) {
		logger.info("******** Checking Content Length *********");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content-Length ===> " + contentLength);
		if(contentLength == null)
			logger.info("Content Length is null");
		else {
			if(Integer.parseInt(contentLength) < Integer.parseInt(contentLengthParam))
				logger.warning("Content Length is less than 50");
			Assert.assertTrue(Integer.parseInt(contentLength) >= Integer.parseInt(contentLengthParam));	
		}
	}
	public void checkCookies(String cookiesParam) {
		logger.info("******** Checking Cookie *********");
		
		String cookie = response.getCookie("PHPSESSID");
		System.out.println(cookie);
		Assert.assertEquals(cookie, cookiesParam);
	}
	public void jsonValidator(String name) {
		logger.info("******** Validating JSON *********");
		JSONObject jsonSchema = new JSONObject(
			      new JSONTokener(bayarPbb.class.getResourceAsStream(name)));
		JSONArray jsonSubject = new JSONArray(
			      new JSONTokener(response.getBody().asString()));
			     
		Schema schema = SchemaLoader.load(jsonSchema);
		schema.validate(jsonSubject);		 
	}
	public void jsonValidatorSingleObject(String name) {
		JSONObject jsonSchema = new JSONObject(
			      new JSONTokener(bayarPbb.class.getResourceAsStream("/division.json")));
		JSONObject jsonSubject = new JSONObject(
			      new JSONTokener(response.getBody().asString()));
		JSONArray jsonArray = new JSONArray();
		jsonSubject.put("id",jsonArray);
		 
		Schema schema = SchemaLoader.load(jsonSchema);
		schema.validate(jsonArray);	
	}
}
