package com.apiTest.bayarPbb.user.verification.email.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Email_Verification extends bayarPbb{
	@BeforeClass
	private void getAllUserData() throws InterruptedException{
		logger.info("******** Email Verification Started TC_001_Email_Verification *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.GET, "/email-verification?email="+akun.getProperty("email")+"&tokenEmail="+akun.getProperty("tokenEmail"));

		Thread.sleep(5);
	}

	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
		Assert.assertTrue(response.getBody().asString().equals("Your email has been activated."));
//		
//		checkResponseBody(akun.getProperty("email"));
//		checkResponseBody(akun.getProperty("idUser"));
	}
	
	
	@Test
	private void checkStatusCodePost() {
		checkStatusCodeNormal("200");
	}
	
	@Test
	private void checkResponseTimePost() {
		checkResponseTime("5000");
		
	}

	@Test
	private void checkStatusLinePost() {
		checkStatusLine("HTTP/1.1 200 ");
	}
	
	@Test
	private void checkContentTypePost() {
		checkContentType("text/plain;charset=UTF-8");
	}

	@Test
	private void checkContentLength() {
		checkContentLength("30");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Email Verification Finished TC_001_Email_Verification *********");
		
	}
}
