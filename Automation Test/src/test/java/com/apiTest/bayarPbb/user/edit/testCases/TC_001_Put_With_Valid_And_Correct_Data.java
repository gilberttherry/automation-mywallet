package com.apiTest.bayarPbb.user.edit.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.reporters.EmailableReporter;

import com.apiTest.bayarPbb.base.bayarPbb;
import com.apiTest.bayarPbb.utilities.RestUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Put_With_Valid_And_Correct_Data extends bayarPbb{
	private String name= RestUtils.nameGenerator();
	private String gmail= RestUtils.gmailGenerator();
	private String password = RestUtils.passwordGenerator();
	private String telephone;
	
	@BeforeClass
	private void putUserData() throws InterruptedException{
		logger.info("******** Update User Data Started TC_001_Put_With_Valid_And_Correct_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		telephone=RestUtils.telephoneNumber(akun.getProperty("telephone"));
		System.out.println(telephone);
		requestParams.put("name", name);
		requestParams.put("email", gmail);
		requestParams.put("password", akun.getProperty("password"));
		requestParams.put("newPassword", password);
		requestParams.put("retryPassword", password);
		requestParams.put("telephone", telephone);
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.PUT, "/user/"+akun.getProperty("idUser")+"?token="+akun.getProperty("token"));

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		akun.setProperty("name", name);
		akun.setProperty("email", gmail);
		akun.setProperty("password", password);
		akun.setProperty("telephone", telephone);
		setProperties(akun, "Akun.properties");
		
		checkResponseBody(name);
	}
	

	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCode) {
		checkStatusCode(successStatusCode);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentTypeParam")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Update User Data Finished TC_001_Put_With_Valid_And_Correct_Data *********");
		
	}
}
