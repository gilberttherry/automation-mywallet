package com.apiTest.bayarPbb.user.getAll.testCases;

import java.util.HashMap;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_001_Get_All_User extends bayarPbb{
	@BeforeClass
	private void getAllUserData() throws InterruptedException{
		logger.info("******** Show User Data Started TC_001_Get_With_Correct_And_Valid_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		org.json.simple.JSONObject requestParams = new org.json.simple.JSONObject();
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.GET, "/users");

		Thread.sleep(20);
	}

	
	@Test
	private void checkResponseBodyPost() {
		//System.out.println(response.getBody().asString());

		getAttribute();
//		checkResponseBody(akun.getProperty("email"));
//		checkResponseBody(akun.getProperty("idUser"));
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCodeParam) {
		
		checkStatusCode(successStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Show User Data Finished TC_001_Get_With_Correct_And_Valid_Data *********");
		
	}
}
