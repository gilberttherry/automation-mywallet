package com.apiTest.bayarPbb.user.verification.newTelephone.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Correct_And_Valid_Data extends bayarPbb{
	
	@BeforeClass
	private void PostLoginData() throws InterruptedException{
		logger.info("******** Started OTP Validation TC_001_Post_With_Valid_And_Registered_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();

		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		requestParams.put("otp", Integer.parseInt(akun.getProperty("otp")));
		
		System.out.println(akun.getProperty("otp"));
		
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		System.out.println(requestParams.toString());
		response = httpRequest.request(Method.POST, "/user/"+akun.getProperty("idUser")+"/otp-new-telephone?token="+akun.getProperty("token"));

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		
		checkResponseBody("Your account has been successfully activated");
	}
	

	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCode) {
		checkStatusCode(successStatusCode);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentTypeParam")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	
//	@Test
//	private void checkServerTypePost() {
//		checkServerType("null");
//	}
//	
//	@Test
//	private void checkContentEncodingPost() {
//		checkContentEncoding("null");
//	}
//	
	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished OTP Validation TC_001_Post_With_Valid_And_Registered_Data *********");
		
	}
}
