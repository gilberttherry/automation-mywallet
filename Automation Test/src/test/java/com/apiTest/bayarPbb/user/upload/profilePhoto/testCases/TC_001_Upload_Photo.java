package com.apiTest.bayarPbb.user.upload.profilePhoto.testCases;

import java.io.File;
import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Upload_Photo extends bayarPbb{
	
	@BeforeClass
	private void uploadPhoto() throws InterruptedException{
		logger.info("******** Upload Profile Photo Started TC_001_Upload_Photo *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		File image = new File("/Users/therryg/eclipse-workspace/FinalProject/src/test/java/com/apiTest/bayarPbb/user/upload/profilePhoto/testCases/photo.jpeg");

		
		httpRequest.multiPart("photo", image,"image/jpeg");
		//requestParams.put("photo", httpRequest.multiPart(image));
		
		//httpRequest.contentType("image/jpg");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.PUT, "/user/"+akun.getProperty("idUser")+"/upload?token="+akun.getProperty("token"));
		System.out.println(requestParams.toString());
		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		String responseBody = response.getBody().asString();
		System.out.println(responseBody);
		
		checkResponseBody("Upload success");
	}
	
	
	@Test
	private void checkStatusCodePost() {
		checkStatusCode("200");
	}
	
	@Test
	private void checkResponseTimePost() {
		checkResponseTime("5000");
		
	}

	@Test
	private void checkStatusLinePost() {
		checkStatusLine("HTTP/1.1 200 ");
	}
	
	@Test
	private void checkContentTypePost() {
		checkContentType("application/json");
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Upload Profile Photo Finished TC_001_Upload_Photo *********");
		
	}
}
