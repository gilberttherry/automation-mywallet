package com.apiTest.bayarPbb.taxBill.create.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;
import com.apiTest.bayarPbb.utilities.RestUtils;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Create_PBB extends bayarPbb{
	private String taxNumber = RestUtils.taxNumberGenerator();
	private Double amount = Double.parseDouble(RestUtils.amountGenerator());
	private int year = RestUtils.yearGenerator();
	private int idCity = RestUtils.idCityGenerator();
	@BeforeClass
	private void CreatePBB() throws InterruptedException{
		logger.info("******** Started Create PBB TC_001_Create_PBB *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("taxNumber", taxNumber);
		requestParams.put("amount", amount);
		requestParams.put("year", year);
		requestParams.put("idCity", idCity);
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/new-pbb");

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
		
		Properties tax = new Properties();
		tax = getProperties("Tax.properties");
		tax.setProperty("taxNumber", taxNumber);
		tax.setProperty("amount", Double.toString(amount));
		tax.setProperty("year", Integer.toString(year));
		tax.setProperty("idCity", Integer.toString(idCity));
		tax.setProperty("idProvince", Integer.toString(1));
		setProperties(tax, "Tax.properties");
		
		checkResponseBody("A new pbb tax has been created.");
		
	}
	
	
	@Test
	@Parameters("postStatusCode")
	private void checkStatusCodePost(String successStatusCodeParam) {
		checkStatusCode(successStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}
//	
//	@Test
//	private void checkServerTypePost() {
//		checkServerType("null");
//	}
//	
//	@Test
//	private void checkContentEncodingPost() {
//		checkContentEncoding("null");
//	}
//	
	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Finished CreatePBB TC_001_Create_PBB *********");
		
	}
}
