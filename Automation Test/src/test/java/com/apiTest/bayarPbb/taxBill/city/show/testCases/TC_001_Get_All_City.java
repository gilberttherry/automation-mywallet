package com.apiTest.bayarPbb.taxBill.city.show.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_All_City extends bayarPbb{
	@BeforeClass
	private void PostRegistrationData() throws InterruptedException{
		logger.info("******** Show City Data Started TC_001_Get_All_City *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/province/"+getIdProvince()+"/city");

		Thread.sleep(5);
	}
	@Test
	private void checkResponseBodyPost() {
		checkResponseBody("Serang");
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCodeParam ) {
		checkStatusCode(successStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	private void checkStatusLinePost() {
		checkStatusLine("HTTP/1.1 200 ");
	}
	
	@Test
	private void checkContentTypePost() {
		checkContentType("application/json");
	}

	@Test
	private void checkContentLength() {
		checkContentLength("150");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Show City Data Finished TC_001_Get_All_City *********");
		
	}
}
