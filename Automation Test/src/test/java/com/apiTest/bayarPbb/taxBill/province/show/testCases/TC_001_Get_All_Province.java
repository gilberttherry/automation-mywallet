package com.apiTest.bayarPbb.taxBill.province.show.testCases;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import freemarker.cache.StrongCacheStorage;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_All_Province extends bayarPbb{
	@BeforeClass
	private void PostRegistrationData() throws InterruptedException{
		logger.info("******** Show Province Data Started TC_001_Get_All_Bank *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/province");

		Thread.sleep(5);
	}
	@Test
	private void checkResponseBodyPost() {
		checkResponseBody("Jakarta");
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCode) {
		checkStatusCode(successStatusCode);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentTypeParam")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("150");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Show Province Data Finished TC_001_Get_All_Province *********");
		
	}
}
