package com.apiTest.bayarPbb.taxBill.show.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Valid_And_Correct_Request_Data extends bayarPbb{
	@BeforeClass
	private void showBillData() throws InterruptedException{
		logger.info("******** Show Bill Data Started TC_001_Post_With_Valid_And_Correct_Request_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		Properties tax = new Properties();
		tax = getProperties("Tax.properties");
		
		requestParams.put("taxNumber", tax.getProperty("taxNumber"));
		requestParams.put("idCity", Integer.parseInt(tax.getProperty("idCity")));
		requestParams.put("idProvince", Integer.parseInt(tax.getProperty("idProvince")));
		requestParams.put("year", Integer.parseInt(tax.getProperty("year")));
		

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/user/"+akun.getProperty("idUser")+"/tax-payment/?token="+akun.getProperty("token"));

		Thread.sleep(5);
	}

	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());
		setPayAmount();
		Properties transaction = new Properties();
		transaction = getProperties("Transaction.properties");
		transaction.setProperty("payAmount", String.valueOf(getPayAmount()));
		setProperties(transaction, "Transaction.properties");
		checkResponseBody("Tax bill is available");
	}
	

	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCode) {
		checkStatusCode(successStatusCode);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentTypeParam")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Show Bill Data Finished TC_001_Post_With_Valid_And_Correct_Request_Data *********");
		
	}
}
