package com.apiTest.bayarPbb.registration.requestOtp.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_002_Post_With_Unvalid_Data extends bayarPbb{
	
	@BeforeClass
	private void postRequestOTP() throws InterruptedException{
		logger.info("******** Request OTP Started TC_002_Post_With_Unvalid_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		requestParams.put("telephone", akun.getProperty("telephone"));
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/register/request-activation");

		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		System.out.println(response.getBody().asString());

		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		String responseBody = response.getBody().asString();
		Assert.assertTrue(responseBody.contains(akun.getProperty("idUser")));
		checkResponseBody("OTP has been sent to your phone.");
	}
	
	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCodeParam) {
		checkStatusCode(successStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("20");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Registration Finished TC_002_Post_With_Unvalid_Data *********");
		
	}
}
