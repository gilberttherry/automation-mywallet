package com.apiTest.bayarPbb.registration.register.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_Valid_and_Correct_Data extends bayarPbb{
	
	@BeforeClass
	private void PostRegistrationData() throws InterruptedException{
		logger.info("******** Registration Started TC_001_Post_Valid_and_Correct_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		requestParams.put("name", akun.getProperty("name"));
		requestParams.put("email", akun.getProperty("email"));
		requestParams.put("telephone", akun.getProperty("telephone"));
		requestParams.put("password", akun.getProperty("password"));
		requestParams.put("retryPassword",akun.getProperty("password"));
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.POST, "/register");
		System.out.println(requestParams.toString());
		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		//String responseBody = response.getBody().asString();

		//System.out.println(responseBody);
		logger.info("Response Body ===>" + response.getBody().asString());
		setIdUser();
		System.out.println(getIdUser());
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		akun.setProperty("idUser",Integer.toString(getIdUser()));
		setProperties(akun, "Akun.properties");

	    System.out.println(akun.getProperty("idUser"));  

		checkResponseBody("Registration is completed.");
	}
	
	
	@Test
	@Parameters("postStatusCode")
	private void checkStatusCodePost(String postStatusCodeParam) {
		checkStatusCode(postStatusCodeParam);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentType")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Registration Finished TC_001_Post_Valid_and_Correct_Data *********");
		
	}
}
