package com.apiTest.bayarPbb.registration.register.testCases;

import java.io.IOException;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;
import com.apiTest.bayarPbb.utilities.Utility;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_002_Post_Negative_Data_With_Excel extends bayarPbb{
	
	@BeforeClass
	private void postLoginData() throws InterruptedException{
		logger.info("******** Registration Started TC_002_Post_Negative_Data_With_Excel *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();

		Thread.sleep(3);
	}
	
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "dataemployes", priority = 0)
	private void dataDrivenPost(String name, String email,String phoneNumber, String password, String retryPassword) throws InterruptedException {
		httpRequest = RestAssured.given();

		JSONObject RequestParams = new JSONObject();
		RequestParams.put("name",name);
		RequestParams.put("email", email);
		RequestParams.put("telephone",phoneNumber);
		RequestParams.put("password", password);
		RequestParams.put("retryPassword",retryPassword);
		
		httpRequest.header("Content-Type","application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		
		response = httpRequest.request(Method.POST, "register");
		System.out.println(response.getBody().asString());
		
		Thread.sleep(5);
	}
	
	@DataProvider(name="dataemployes")
	private String[][] getDataEmpty() throws IOException {
		String path = "/Users/therryg/eclipse-workspace/FinalProject/src/test/java/com/apiTest/bayarPbb/registration/register/testCases/Negative_Data.xlsx";
		int rownum =Utility.getRowCount(path, "Sheet1");
		int colcount = Utility.getCellCount(path, "Sheet1",1);
		String dataEmployes [][] = new String[rownum][colcount];
		for(int i=1;i<=rownum;i++) {
			for(int j=0;j<colcount;j++) {
				dataEmployes[i-1][j] = Utility.getCellData(path,"Sheet1",i,j);
			}
		}
		return(dataEmployes);
	}

//	@Test(priority = 1)
//	private void checkResponseBodyPost() {
//		checkResponseBodyDynamicPostLogin("Email does not exist");
//	}
	
	
	@Test(priority = 1)
	private void checkStatusCodePost() {
		checkStatusCode("500");
	}
	
	@Test(priority = 1)
	private void checkResponseTimePost() {
		checkResponseTime("5000");
		
	}

	@Test(priority = 1)
	private void checkStatusLinePost() {
		checkStatusLine("HTTP/1.1 500 ");
	}
	
	@Test(priority = 1)
	private void checkContentTypePost() {
		checkContentType("application/json");
	}
	
//	@Test(priority = 1)
//	private void checkServerTypePost() {
//		checkServerType("\0");
//	}
	
//	@Test(priority = 1)
//	private void checkContentEncodingPost() {
//		checkContentEncoding("\0");
//	}

	@AfterClass
	private void tearDown() {
		logger.info("******** Registration TC_002_Post_Negative_Data_With_Excel *********");
		
	}
}
