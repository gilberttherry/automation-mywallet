package com.apiTest.bayarPbb.updatePin.testCases;

import java.util.Properties;

import org.json.simple.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.apiTest.bayarPbb.base.bayarPbb;

import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Post_With_Valid_And_Correct_Data extends bayarPbb{
	
	@BeforeClass
	private void putPin() throws InterruptedException{
		logger.info("******** Update Pin Started TC_001_Post_With_Valid_And_Correct_Data *********");
		RestAssured.baseURI = baseUrlString;
		httpRequest = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		
		Properties akun = new Properties();
		akun = getProperties("Akun.properties");
		
		requestParams.put("pin", Integer.parseInt(akun.getProperty("pin")));
		requestParams.put("retryPin", Integer.parseInt(akun.getProperty("pin")));
		
		httpRequest.contentType("application/json");
		httpRequest.body(requestParams.toJSONString());
		response = httpRequest.request(Method.PUT, "/user/"+akun.getProperty("idUser")+"/update-pin?token="+akun.getProperty("token"));
		
		Thread.sleep(5);
	}
	
	@Test
	private void checkResponseBodyPost() {
		//System.out.println(response.getBody().asString());
		checkResponseBody("Your pin is successfully updated");
	}

	
	@Test
	@Parameters("successStatusCode")
	private void checkStatusCodePost(String successStatusCode) {
		checkStatusCode(successStatusCode);
	}
	
	@Test
	@Parameters("responseTime")
	private void checkResponseTimePost(String responseTimeParam) {
		checkResponseTime(responseTimeParam);
		
	}

	@Test
	@Parameters("statusLine")
	private void checkStatusLinePost(String statusLineParam) {
		checkStatusLine(statusLineParam);
	}
	
	@Test
	@Parameters("contentTypeParam")
	private void checkContentTypePost(String contentTypeParam) {
		checkContentType(contentTypeParam);
	}

	@Test
	private void checkContentLength() {
		checkContentLength("50");
	}
	
//	@Test
//	@Parameters("serverType")
//	private void checkServerTypePost(String serverTypeParam) {
//		checkServerType(serverTypeParam);
//	}
//	
//	@Test
//	@Parameters("contentEncodingNull")
//	private void checkContentEncodingPost(String contentEncodingParam) {
//		checkContentEncoding(contentEncodingParam);
//	}
//	
	
	
	@AfterClass
	private void tearDown() {
		logger.info("******** Update Pin Finished TC_001_Post_With_Valid_And_Correct_Data *********");
		
	}
}
